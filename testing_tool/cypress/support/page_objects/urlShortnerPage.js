export class UrlShortnerPage {

    addressBarCheck() {
        cy.url().should('include', '3000').log('Website Loaded')
    }

    elementsCheck() {
        cy.get('header').should('be.visible').log('Element of type HEADER is visible')
        cy.get('input').should('be.visible').log('Element of type INPUT is visible')
    }

    getContentOf(address) {
        cy.get('input[type="url"]').type(address)
        cy.get('button').should('contain', 'Submit').click()
        
        const linkResponse = cy.get('a.App-link').then(($a) => {
            const url = $a.prop('href')
            cy.request(url).its('body')
        })
        
        if (address.startsWith('http://') || address.startsWith('https://')) {
            return linkResponse.should('include', '</html>')
        } else {
            return linkResponse.then(content => {
                expect(content.error).to.eq('address not found')
            })
        }
    }

}

export const onUrlShortnerPage = new UrlShortnerPage();