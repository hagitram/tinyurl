context('TinyURL - API Tests', () => {

    describe('Shortening URL', () => {
        
        it('Should return status 201', () => {
            cy.intercept('POST', '/tinyurl/', (req) => {})
        })
        
        it('Should receive long URL and return short token', () => {})
    })
    
    describe('Redirecting to long URL', () => {
        
        it('Should return status 300', () => {
            cy.intercept('GET')
        })
        
        it('Should return status 500', () => {})
        
    })
    
    describe('Retrieving original URL as JSON', () => {
        
        it('Should return status 200', () => {})
        
        it('Should return status 500', () => {})
        
        it('Should receive short token and return long URL', () => {})
    })
})