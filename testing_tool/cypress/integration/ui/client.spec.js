import { onUrlShortnerPage } from '../../support/page_objects/urlShortnerPage';

context('TinyURL - UI Tests', () => {

    beforeEach('Open application', () => {
        cy.visit('/')
    })

    describe('Sanity', () => {

        it('Should load the application', () => {
            onUrlShortnerPage.addressBarCheck()
        })

        it('Should check for elements on the page', () => {
            onUrlShortnerPage.elementsCheck()
        })
    })

    describe('Positive scenario', () => {

        it('Should redirect to a valid URL', () => {
            onUrlShortnerPage.getContentOf('https://movizz.netlify.app/')
        })

        it('Should redirect to a valid URL', () => {
            onUrlShortnerPage.getContentOf('https://youtube.com')
        })
    })

    describe('Negative scenario', () => {

        it('Should display ERROR message due to invalid URL', () => {
            onUrlShortnerPage.getContentOf('HGJjgh')
        })

        it('Should display ERROR message due to invalid URL', () => {
            onUrlShortnerPage.getContentOf('mysite.com')
        })
    })

    describe('Edge cases', () => {

        it('Should display ERROR message', () => {
            onUrlShortnerPage.getContentOf('youtube.com')
        })

        it('Should display ERROR message', () => {
            onUrlShortnerPage.getContentOf('http://')
        })
        
        it('Should display ERROR message', () => {
            onUrlShortnerPage.getContentOf('https://')
        })

        it('Should display ERROR message', () => {
            onUrlShortnerPage.getContentOf('https://kjfhdJAL')
        })
    })
})