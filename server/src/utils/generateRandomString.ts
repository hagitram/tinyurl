const generateRandomString = (outputLength: number) => {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let randomString = '';
    for (let index = 0; index < outputLength; index++) {
        randomString = randomString.concat(chars.charAt(Math.floor(Math.random() * chars.length)))
    }
    return randomString;
};

export default generateRandomString;