import { RequestHandler } from 'express';
import generateRandomString from '../utils/generateRandomString';

interface JSON {
    [key: string]: string
}

const cache: JSON = {};

export const shortenURL: RequestHandler = (req, res, next) => {
    const longURL = (req.body as { longURL: string }).longURL;

    const shortToken: String = generateRandomString(10);

    cache[`${shortToken}`] = longURL;

    res.status(201).json({ longURL: longURL, shortToken: shortToken });

    //console.log(`POST request sent with ${longURL}, received: ${shortToken}, cache: ${JSON.stringify(cache)}`);
};

export const redirectURL: RequestHandler = (req, res, next) => {
    const shortToken = req.params.shortToken;

    cache.hasOwnProperty(shortToken) ?
        res.redirect(cache[`${shortToken}`]) :
        res.status(500).json({ error: 'address not found' });

    //console.log(`GET request sent with ${shortToken}, cache.hasOwnProperty(shortToken): ${cache.hasOwnProperty(shortToken)}`);
};

export const getURL: RequestHandler = (req, res, next) => {
    const shortToken = req.params.shortToken;

    cache.hasOwnProperty(shortToken) ?
        res.status(200).json({ orinigalURL: cache[`${shortToken}`] }) :
        res.status(500).json({ error: 'address not found' })
};