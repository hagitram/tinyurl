import express, { Request, Response, NextFunction } from 'express';

import bodyParser from 'body-parser';

import tinyurlRoutes from './routes/tinyUrl';

const app = express();

app.use(bodyParser.json());

app.use('/tinyurl', tinyurlRoutes);

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    res.status(500).json({ message: err.message });
});

app.listen(5000);