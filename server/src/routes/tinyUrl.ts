import { Router } from 'express';

import { shortenURL, redirectURL, getURL } from '../controllers/tinyUrl';

const router = Router();

router.post('/', shortenURL);

router.get('/:shortToken', redirectURL);

router.get('/origin/:shortToken', getURL);

export default router;