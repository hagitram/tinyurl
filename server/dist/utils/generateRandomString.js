"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const generateRandomString = (outputLength) => {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let randomString = '';
    for (let index = 0; index < outputLength; index++) {
        randomString = randomString.concat(chars.charAt(Math.floor(Math.random() * chars.length)));
    }
    return randomString;
};
exports.default = generateRandomString;
