"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getURL = exports.redirectURL = exports.shortenURL = void 0;
const generateRandomString_1 = __importDefault(require("../utils/generateRandomString"));
const cache = {};
const shortenURL = (req, res, next) => {
    const longURL = req.body.longURL;
    const shortToken = (0, generateRandomString_1.default)(10);
    cache[`${shortToken}`] = longURL;
    res.status(201).json({ longURL: longURL, shortToken: shortToken });
    //console.log(`POST request sent with ${longURL}, received: ${shortToken}, cache: ${JSON.stringify(cache)}`);
};
exports.shortenURL = shortenURL;
const redirectURL = (req, res, next) => {
    const shortToken = req.params.shortToken;
    cache.hasOwnProperty(shortToken) ?
        res.redirect(cache[`${shortToken}`]) :
        res.status(200).json({ error: 'address not found' });
    //console.log(`GET request sent with ${shortToken}, cache.hasOwnProperty(shortToken): ${cache.hasOwnProperty(shortToken)}`);
};
exports.redirectURL = redirectURL;
const getURL = (req, res, next) => {
    const shortToken = req.params.shortToken;
    res.status(200).json({ orinigalURL: cache.hasOwnProperty(shortToken) ? cache[`${shortToken}`] : '' });
};
exports.getURL = getURL;
