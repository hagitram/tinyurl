"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const tinyUrl_1 = require("../controllers/tinyUrl");
const router = (0, express_1.Router)();
router.post('/', tinyUrl_1.shortenURL);
router.get('/:shortToken', tinyUrl_1.redirectURL);
router.get('/origin/:shortToken', tinyUrl_1.getURL);
exports.default = router;
