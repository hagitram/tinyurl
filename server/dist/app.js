"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const tinyUrl_1 = __importDefault(require("./routes/tinyUrl"));
const app = (0, express_1.default)();
app.use(body_parser_1.default.json());
app.use('/tinyurl', tinyUrl_1.default);
app.use((err, req, res, next) => {
    res.status(500).json({ message: err.message });
});
app.listen(5000);
