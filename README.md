# TinyURL


## Requirements

NodeJS v16.14.0


## Getting started

### inside the newly created directory - tinyurl :

it is recommended to work with split terminal:

#### 1.
```
$ cd server

$ npm install

$ npm start
```

#### 2.
```
$ cd ui

$ npm install

$ npm start
```

### to run tests (when server and client are running):
```
$ cd testing_tool

$ npm install

$ npx cypress open
```



## Project status
backend and frontend completely functional,
testing under develompent.



## Dependencies - DO NOT run this commands! (for development use)
npm install -g npm

### ui
npx create-react-app ui 

npm install react-router-dom

### server
npm install -g typescript

npm init

tsc --init

[to run ts compiler in watch mode: tsc -w]

npm install --save express body-parser

npm install --save-dev nodemon @types/node @types/express

### cypress
npm init

npm install cypress

npm install prettier

