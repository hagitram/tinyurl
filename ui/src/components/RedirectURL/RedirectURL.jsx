import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

const RedirectURL = () => {
    const params = useParams();
    const shortToken = params.shortToken;
    const queryForGETrequest = '/tinyurl/origin/';
    const [originalURL, setOriginalURL] = useState('');


    useEffect(() => {
        const getOriginalURL = async () => {
            await fetch(`${queryForGETrequest}${shortToken}`)
                .then(res => res.json())
                .then(body => setOriginalURL(body.orinigalURL));
        }
        getOriginalURL();

        if (originalURL.startsWith("http://") || originalURL.startsWith("https://")) {
            window.location.href = originalURL;
        }
    });

    return <>Redirected URL -&gt; {originalURL}</>
}

export default RedirectURL;