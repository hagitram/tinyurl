import React, { useState, useEffect } from 'react';
import OutputURL from '../OutputURL/OutputURL';

import './InputURL.css';

const InputURL = () => {
    const [outputVisible, setOutputVisible] = useState(false);
    const [inputContent, setInputContent] = useState('');
    const [longURL, setLongURL] = useState('');
    const [shortToken, setShortToken] = useState('');

    useEffect(() => {
        const postRequestOptions = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                longURL: longURL
            })
        };

        if (longURL) {
            fetch('/tinyurl', postRequestOptions)
                .then(res => res.json())
                .then(body => {
                    setShortToken(body.shortToken);
                });
            setOutputVisible(true);
        }
    }, [longURL])

    const handleClick = () => {
        setLongURL(inputContent);
    }

    return (
        <div className="App">
            <header className="App-header">
                <div className='input-url-container'>
                    Please insert URL: &nbsp;
                    <input
                        type="url"
                        onChange={e => setInputContent(e.target.value)}
                        value={inputContent}
                        onKeyUp={e => e.key === 'Enter' && handleClick()}
                    />
                    <button onClick={handleClick}>Submit</button>
                </div>
                {outputVisible && <OutputURL shortToken={shortToken} />}
            </header>
        </div>
    )
}

export default InputURL;
