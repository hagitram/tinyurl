import React from "react";

const OutputURL = (props) => {
    const { shortToken } = props;

    const baseURL = 'http://localhost:5000';
    const route = '/tinyurl/';
    const shortURL = `${route}${shortToken}`;

    return (
        <>
            <p>
                The address you've typed can be found under:
            </p>
            <a
                className="App-link"
                href={`${baseURL}${shortURL}`}
            >
                {shortURL}
            </a>
        </>
    )
}

export default OutputURL;