import { BrowserRouter, Routes, Route } from 'react-router-dom';
import InputURL from './components/InputURL/InputURL';
import RedirectURL from './components/RedirectURL/RedirectURL';

import './App.css';

function App() {
  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<InputURL />} />
          <Route path="/tinyurl/:shortToken" element={<RedirectURL />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;
